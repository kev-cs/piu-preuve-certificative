package com.kev.stocksapp;


import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import com.kev.stocksapp.controller.MainActivity;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers
        .withContentDescription;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class ExploreFragmentTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule =
            new ActivityTestRule<>(MainActivity.class);

    @Test
    public void mainActivityTest3() {
        ViewInteraction appCompatImageButton =
                onView(allOf(withContentDescription("Open navigation drawer"),
                        withParent(withId(R.id.toolbar)), isDisplayed()));
        appCompatImageButton.perform(click());

        ViewInteraction appCompatCheckedTextView =
                onView(allOf(withId(R.id.design_menu_item_text),
                        withText("Help"), isDisplayed()));
        appCompatCheckedTextView.perform(click());

        ViewInteraction textView =
                onView(allOf(withId(R.id.textView), withText("Help"),
                        childAtPosition(childAtPosition(
                                withId(R.id.content_navigationdrawer), 0), 0),
                        isDisplayed()));
        textView.check(matches(withText("Help")));

        ViewInteraction textView2 = onView(allOf(withText("Explore Section"),
                childAtPosition(
                        childAtPosition(withId(R.id.content_navigationdrawer),
                                0), 1), isDisplayed()));
        textView2.check(matches(withText("Explore Section")));

        ViewInteraction textView3 = onView(allOf(withText(
                "The explore section is for you to explore! You have a top 10"
                        + " stocks tab, a indices tab and a currencies tab. "
                        + "You can tap on an item to see the details of the "
                        + "quote."),
                childAtPosition(
                        childAtPosition(withId(R.id.content_navigationdrawer),
                                0), 2), isDisplayed()));
        textView3.check(matches(withText(
                "The explore section is for you to explore! You have a top 10"
                        + " stocks tab, a indices tab and a currencies tab. "
                        + "You can tap on an item to see the details of the "
                        + "quote.")));

        ViewInteraction textView4 = onView(allOf(withText("Watchlist Section"),
                childAtPosition(
                        childAtPosition(withId(R.id.content_navigationdrawer),
                                0), 3), isDisplayed()));
        textView4.check(matches(withText("Watchlist Section")));

        ViewInteraction textView5 = onView(allOf(withText(
                "This section is a customizable watchlist. You can add quotes"
                        + " by pressing the + button or you can remove items "
                        + "by long pressing. Once you've added an item, tap "
                        + "on it to see the details."),
                childAtPosition(
                        childAtPosition(withId(R.id.content_navigationdrawer),
                                0), 4), isDisplayed()));
        textView5.check(matches(withText(
                "This section is a customizable watchlist. You can add quotes"
                        + " by pressing the + button or you can remove items "
                        + "by long pressing. Once you've added an item, tap "
                        + "on it to see the details.")));

        ViewInteraction textView6 = onView(allOf(withText("Help Section"),
                childAtPosition(
                        childAtPosition(withId(R.id.content_navigationdrawer),
                                0), 5), isDisplayed()));
        textView6.check(matches(withText("Help Section")));

        ViewInteraction textView7 =
                onView(allOf(withText("You're in it! It's to help you!"),
                        childAtPosition(childAtPosition(
                                withId(R.id.content_navigationdrawer), 0), 6),
                        isDisplayed()));
        textView7.check(matches(withText("You're in it! It's to help you!")));

    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText(
                        "Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher
                        .matches(parent) && view
                        .equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
