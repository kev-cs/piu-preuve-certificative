package com.kev.stocksapp.controller.explore;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.kev.stocksapp.controller.ViewQuoteActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ExplorePagerAdapter extends PagerAdapter {

    private Map<String, List<String>> exploreTypesMap;
    private Context context;
    private List<String> titles;

    public ExplorePagerAdapter(Context context,
            Map<String, List<String>> exploreTypesLists) {
        this.context = context;
        titles = new ArrayList<>(exploreTypesLists.keySet());
        this.exploreTypesMap = exploreTypesLists;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        final List<String> symbols = exploreTypesMap.get(titles.get(position));
        ListAdapter listAdapter =
                new ArrayAdapter<>(context, android.R.layout.simple_list_item_1,
                        symbols);
        ListView listView = new ListView(context);
        listView.setAdapter(listAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int pos,
                    long id) {
                Intent intent = new Intent(context, ViewQuoteActivity.class);
                String symbol = symbols.get(pos);
                intent.putExtra(ViewQuoteActivity.EXTRA_SYMBOL, symbol);
                context.startActivity(intent);
            }
        });
        container.addView(listView);
        return listView;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public int getCount() {
        return exploreTypesMap.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles.get(position);
    }

    @Override
    public void destroyItem(View container, int position, Object object) {
        ((ViewPager) container).removeView((View) object);
    }

}
