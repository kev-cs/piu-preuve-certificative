package com.kev.stocksapp.controller.watchlist;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.kev.stocksapp.R;
import com.kev.stocksapp.model.Quote;

import java.util.List;


public class WatchlistItemListAdapter extends ArrayAdapter<Quote> {

	public WatchlistItemListAdapter(Context context, int resource,
			List<Quote> objects) {
		super(context, resource, objects);
	}

	@NonNull
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		if(view == null){
			LayoutInflater inflater = LayoutInflater.from(getContext());
			view = inflater.inflate(R.layout.portfolio_item, null);
		}

		Quote quote = getItem(position);
		if (quote != null) {
			TextView symbol = (TextView) view.findViewById(R.id.item_symbol);
			TextView name = (TextView) view.findViewById(R.id.item_name);
			TextView lastTradePrice = (TextView) view.findViewById(R.id
					.last_trade_price);
			TextView change = (TextView) view.findViewById(R.id.change);

			symbol.setText(quote.getSymbol());
			name.setText(quote.getName());
			lastTradePrice.setText(quote.getLastTradePriceOnly().toString());
			String changeStr = quote.getChange();
			change.setText(changeStr);

		}

		return view;
	}
}
