package com.kev.stocksapp.controller.explore;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kev.stocksapp.R;
import com.kev.stocksapp.StocksApplication;
import com.kev.stocksapp.model.repository.explore.ExploreRepository;

import java.util.List;
import java.util.Map;


public class ExploreFragment extends Fragment {

    private ViewPager viewPager;
    private TabLayout tabLayout;
    private ExplorePagerAdapter adapter;
    private ExploreRepository exploreRepo;

    public interface OnFragmentInteractionListener {}

    public ExploreFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view =
                inflater.inflate(R.layout.fragment_explore, container, false);
        initRepos();
        initViewPager(view);
        return view;
    }

    private void initRepos() {
        StocksApplication application =
                (StocksApplication) getActivity().getApplication();
        this.exploreRepo = application.getExploreRepository();
    }

    private void initViewPager(View view) {
        Map<String, List<String>> exploreTypesLists =
                exploreRepo.getExploreTypesLists();

        viewPager = (ViewPager) view.findViewById(R.id.explore_view_pager);
        adapter = new ExplorePagerAdapter(this.getContext(), exploreTypesLists);
        viewPager.setAdapter(adapter);
        tabLayout = (TabLayout) view.findViewById(R.id.explore_tabs);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.addOnPageChangeListener(
                new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
    }

}
