package com.kev.stocksapp.controller.watchlist;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.kev.stocksapp.R;
import com.kev.stocksapp.StocksApplication;
import com.kev.stocksapp.controller.ViewQuoteActivity;
import com.kev.stocksapp.model.Quote;
import com.kev.stocksapp.model.repository.quote.StockQuoteRepoCallback;
import com.kev.stocksapp.model.repository.quote.StockQuoteRepository;
import com.kev.stocksapp.model.repository.watchlist.WatchlistRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;


public class WatchlistFragment extends Fragment implements Observer {
    private static final int REQ_ADD_PORTFOLIO_ITEM = 1;
    public static final String TAG_ADD_PORTFOLIO_ITEM_DIALOG =
            "addPortfolioItemDialog";

    private WatchlistRepository watchlistRepository;
    private WatchlistItemListAdapter listAdapter;
    private ListView watchlistItemsListView;
    private List<Quote> quotes;
    private ProgressDialog progressDialog;
    private StockQuoteRepository stockQuoteRepo;

    public WatchlistFragment() {
    }

    public static WatchlistFragment newInstance() {
        WatchlistFragment fragment = new WatchlistFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
        initRepos();
        watchlistRepository.addObserver(this);
    }

    @Override
    public void onDestroy() {
        watchlistRepository.deleteObserver(this);
        super.onDestroy();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View view =
                inflater.inflate(R.layout.fragment_watchlist, container, false);

        progressDialog = ProgressDialog
                .show(getContext(), getString(R.string.loading_watchlist),
                        getString(R.string.loading_watchlist_msg), true);
        watchlistItemsListView =
                (ListView) view.findViewById(R.id.portfolio_list);
        getQuotesFromWatchlist();
        initAddPortfolioItemFAB(view);
        return view;
    }

    private void getQuotesFromWatchlist() {
        List<String> symbols = watchlistRepository.getWatchlistItemList();
        stockQuoteRepo.getQuotesFromSymbols(symbols,
                new StockQuoteRepoCallback<List<Quote>>() {
                    @Override
                    public void onResponse(List<Quote> quotes) {
                        WatchlistFragment.this.quotes = quotes;
                        progressDialog.dismiss();
                        initWatchlist();
                    }

                    @Override
                    public void notFound() {
                        Toast.makeText(getContext(), R.string.no_quotes_found,
                                Toast.LENGTH_LONG).show();
                        WatchlistFragment.this.quotes = new ArrayList<>();
                        initWatchlist();
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onFailure() {
                        Toast.makeText(getContext(),
                                R.string.error_fetching_quote,
                                Toast.LENGTH_LONG).show();
                        progressDialog.dismiss();
                    }
                });
    }

    private void initWatchlist() {
        listAdapter = new WatchlistItemListAdapter(getContext(),
                R.layout.portfolio_item, quotes);
        watchlistItemsListView.setAdapter(listAdapter);
        watchlistItemsListView
                .setOnItemClickListener(getAdapterOnItemClickListener());
        watchlistItemsListView
                .setOnItemLongClickListener(getListLongClickListener());
    }

    private void initAddPortfolioItemFAB(View view) {
        FloatingActionButton fab =
                (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(getAddPortfolioItemFABListener());
    }

    @NonNull
    private AdapterView.OnItemClickListener getAdapterOnItemClickListener() {
        return new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                    int position, long id) {
                viewPortfolioItemAt(position);
            }

            private void viewPortfolioItemAt(int position) {
                Intent intent =
                        new Intent(getContext(), ViewQuoteActivity.class);
                Quote quote = listAdapter.getItem(position);
                intent.putExtra(ViewQuoteActivity.EXTRA_SYMBOL,
                        quote.getSymbol());
                startActivity(intent);
            }
        };
    }

    @NonNull
    private AdapterView.OnItemLongClickListener getListLongClickListener() {
        return new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view,
                    int position, long id) {
                showDeletePortfolioItemDialog(position);
                return true;
            }
        };
    }

    private void showDeletePortfolioItemDialog(final int position) {
        new AlertDialog.Builder(getContext())
                .setTitle(R.string.delete_watchlist_item_title)
                .setMessage(R.string.delete_watchlist_item_question)
                .setPositiveButton(android.R.string.yes,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,
                                    int which) {
                                String symbol =
                                        quotes.get(position).getSymbol();
                                watchlistRepository.removePortfolioItem(symbol);
                            }
                        }).setNegativeButton(android.R.string.no, null).create()
                .show();
    }

    @NonNull
    private View.OnClickListener getAddPortfolioItemFABListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NewWatchlistItemDialog dialog = new NewWatchlistItemDialog();
                dialog.setTargetFragment(WatchlistFragment.this,
                        REQ_ADD_PORTFOLIO_ITEM);
                dialog.show(getActivity().getSupportFragmentManager(),
                        TAG_ADD_PORTFOLIO_ITEM_DIALOG);
            }
        };
    }

    private void initRepos() {
        StocksApplication application =
                (StocksApplication) getActivity().getApplication();
        this.watchlistRepository = application.getWatchlistRepo();
        this.stockQuoteRepo = application.getStockQuoteRepository();
    }

    @Override
    public void update(Observable observable, Object o) {
        progressDialog = ProgressDialog
                .show(getContext(), getString(R.string.loading_watchlist),
                        getString(R.string.loading_watchlist_msg), true);
        getQuotesFromWatchlist();
    }

}

