package com.kev.stocksapp.controller;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.kev.stocksapp.R;
import com.kev.stocksapp.controller.explore.ExploreFragment;
import com.kev.stocksapp.controller.help.HelpFragment;
import com.kev.stocksapp.controller.watchlist.WatchlistFragment;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        ExploreFragment.OnFragmentInteractionListener {

    private Toolbar toolbar;
    private DrawerLayout drawer;
    private NavigationView navigationView;
    private Map<Integer, Fragment> menuFragmentsMap;
    private Map<Integer, Integer> titlesMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initFragmentsAndTitles();
        initToolbar();
        initDrawer();
        initNavView();
        if(savedInstanceState == null){
            navigationView.setCheckedItem(R.id.menu_item_explore);
            navigateToFragment(new ExploreFragment(), R.string.explore);
        }
    }

    private void initFragmentsAndTitles() {
        menuFragmentsMap = new HashMap<>();
        menuFragmentsMap.put(R.id.menu_item_watchlist, new WatchlistFragment());
        menuFragmentsMap.put(R.id.menu_item_explore, new ExploreFragment());
        menuFragmentsMap.put(R.id.menu_item_help, new HelpFragment());
        titlesMap = new HashMap<>();
        titlesMap.put(R.id.menu_item_watchlist, R.string.watchlist);
        titlesMap.put(R.id.menu_item_explore, R.string.explore);
        titlesMap.put(R.id.menu_item_help, R.string.help);
    }

    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    private void initDrawer() {
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle =
                new ActionBarDrawerToggle(this, drawer, toolbar,
                        R.string.navigation_drawer_open,
                        R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
    }

    private void initNavView() {
        navigationView =
                (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        Fragment fragment = menuFragmentsMap.get(id);
        @StringRes int titleRes = titlesMap.get(id);
        if(fragment == null || titleRes == 0){
            return false;
        }
        navigateToFragment(fragment, titleRes);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void navigateToFragment(Fragment fragment, @StringRes int title) {
        setTitle(title);
        FragmentManager fm = getSupportFragmentManager();
        fm.beginTransaction().replace(R.id.content_navigationdrawer,
                fragment).commit();
    }

}
