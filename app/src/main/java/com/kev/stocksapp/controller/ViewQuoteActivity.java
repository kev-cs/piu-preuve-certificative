package com.kev.stocksapp.controller;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;
import android.widget.Toast;

import com.kev.stocksapp.R;
import com.kev.stocksapp.StocksApplication;
import com.kev.stocksapp.model.Quote;
import com.kev.stocksapp.model.repository.quote.StockQuoteRepoCallback;
import com.kev.stocksapp.model.repository.quote.StockQuoteRepository;

import java.io.IOException;


public class ViewQuoteActivity extends AppCompatActivity {

    public static final String EXTRA_SYMBOL = "symbol";
    private Quote quote;
    private StockQuoteRepository quoteRepository;

    private TextView nameView;
    private TextView stockExchangeNameView;
    private TextView lastTradePriceView;
    private TextView currencyView;
    private TextView askView;
    private TextView bidView;
    private TextView changeView;
    private TextView openView;
    private TextView closeView;
    private TextView percentChangeView;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_quote);
        initQuoteView();
        progressDialog = ProgressDialog
                .show(this, getString(R.string.fetching_quote),
                        getString(R.string.loading_quote_msg), true);
        quoteRepository = ((StocksApplication) getApplication())
                .getStockQuoteRepository();
        try {
            getQuoteFromExtra();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.quote_toolbar);
        toolbar.setTitle(quote.getSymbol());
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void getQuoteFromExtra() throws IOException {
        Intent intent = getIntent();
        String quoteSymbol = intent.getStringExtra(EXTRA_SYMBOL);
        quoteRepository.getSingleQuoteFromSymbol(quoteSymbol,
                new StockQuoteRepoCallback<Quote>() {
                    @Override
                    public void onResponse(Quote quotes) {
                        progressDialog.dismiss();
                        quote = quotes;
                        initQuoteDetails();
                    }

                    @Override
                    public void notFound() {
                        progressDialog.dismiss();
                        Toast.makeText(ViewQuoteActivity.this,
                                getString(R.string.quote_not_found),
                                Toast.LENGTH_LONG).show();
                        finish();
                    }

                    @Override
                    public void onFailure() {
                        progressDialog.dismiss();
                        Toast.makeText(ViewQuoteActivity.this,
                                getString(R.string.error_fetching_quote),
                                Toast.LENGTH_LONG).show();
                        finish();
                    }
                });
    }

    private void initQuoteView() {
        nameView = (TextView) findViewById(R.id.name);
        stockExchangeNameView = (TextView) findViewById(R.id.stock_exchange);
        lastTradePriceView = (TextView) findViewById(R.id.last_trade_price);
        askView = (TextView) findViewById(R.id.ask);
        bidView = (TextView) findViewById(R.id.bid);
        currencyView = (TextView) findViewById(R.id.currency);
        changeView = (TextView) findViewById(R.id.change);
        openView = (TextView) findViewById(R.id.open);
        closeView = (TextView) findViewById(R.id.prev_close);
        percentChangeView = (TextView) findViewById(R.id.percent_change);
    }

    private void initQuoteDetails() {
        nameView.setText(quote.getName());
        stockExchangeNameView.setText(quote.getStockExchange());
        lastTradePriceView.setText(quote.getLastTradePriceOnly().toString());
        askView.setText(quote.getAsk().toString());
        bidView.setText(quote.getBid().toString());
        currencyView.setText(quote.getCurrency());
        changeView.setText(quote.getChange());
        openView.setText(quote.getOpen().toString());
        closeView.setText(quote.getPreviousClose().toString());
        percentChangeView.setText(quote.getChangeinPercent());
        initToolbar();
    }
}
