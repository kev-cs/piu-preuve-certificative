package com.kev.stocksapp.controller.watchlist;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.kev.stocksapp.R;
import com.kev.stocksapp.StocksApplication;
import com.kev.stocksapp.model.Quote;
import com.kev.stocksapp.model.repository.watchlist.WatchlistRepository;
import com.kev.stocksapp.model.repository.quote.StockQuoteRepoCallback;
import com.kev.stocksapp.model.repository.quote.StockQuoteRepository;

import java.io.IOException;

public class NewWatchlistItemDialog extends AppCompatDialogFragment {

    private EditText symbolText;
    private TextView selectedQuoteView;
    private ImageButton searchButton;
    private WatchlistRepository portfolioRepository;
    private StockQuoteRepository stockQuoteRepository;
    private Quote selectedQuote;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View view = LayoutInflater.from(getActivity())
                .inflate(R.layout.new_portfolio_item_dialog, null);
        symbolText = (EditText) view.findViewById(R.id.symbol);
        searchButton = (ImageButton) view.findViewById(R.id.searchButton);
        selectedQuoteView =
                (TextView) view.findViewById(R.id.selected_quote_value);
        searchButton.setOnClickListener(getSearchButtonOnClickListener());
        return buildDialog(view);
    }

    @NonNull
    private View.OnClickListener getSearchButtonOnClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String symbol =
                            NewWatchlistItemDialog.this.symbolText.getText()
                                    .toString();
                    stockQuoteRepository.getSingleQuoteFromSymbol(symbol,
                            getSearchQuoteSymbolCallback());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
    }

    @NonNull
    private StockQuoteRepoCallback<Quote> getSearchQuoteSymbolCallback() {
        return new StockQuoteRepoCallback<Quote>() {
            @Override
            public void onResponse(Quote quote) {
                selectedQuote = quote;
                int quoteFoundColor =
                        getResources().getColor(R.color.colorSuccess);
                selectedQuoteView.setTextColor(quoteFoundColor);
                selectedQuoteView.setText(selectedQuote.getSymbol());
                Toast.makeText(getContext(), R.string.quote_found,
                        Toast.LENGTH_LONG).show();
            }

            @Override
            public void notFound() {
                Toast.makeText(getContext(), R.string.quote_not_found,
                        Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure() {
                Toast.makeText(getContext(), getString(R.string.error_fetching_quote),
                        Toast.LENGTH_LONG).show();
            }
        };
    }

    @Nullable
    @Override
    // TODO manage onSaveState
    public View onCreateView(LayoutInflater inflater,
            @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        initPortfolioRepository();
        initStockQuoteRepository();
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    private void initStockQuoteRepository() {
        StocksApplication application =
                (StocksApplication) getActivity().getApplication();
        stockQuoteRepository = application.getStockQuoteRepository();
    }


    @NonNull
    private DialogInterface.OnShowListener getOnShowListener(
            final AlertDialog alertDialog) {
        return new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Button b = alertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                b.setOnClickListener(getPositiveButtonListener());
            }
        };
    }

    private AlertDialog buildDialog(View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view).setTitle(R.string.new_watchlist_item)
                .setPositiveButton(R.string.add_to_watchlist, null)
                .setNegativeButton(android.R.string.cancel, null);
        AlertDialog dialog = builder.create();
        dialog.setOnShowListener(getOnShowListener(dialog));
        return dialog;
    }

    @NonNull
    private View.OnClickListener getPositiveButtonListener() {
        return new View.OnClickListener() {
            public void onClick(View view) {
                if (selectedQuote != null) {
                    portfolioRepository.addPortfolioItem(selectedQuote.getSymbol());
                    dismiss();
                } else {
                    Toast.makeText(getContext(), R.string.no_quote_err_msg,
                            Toast.LENGTH_LONG).show();
                }
            }
        };
    }


    private void initPortfolioRepository() {
        StocksApplication application =
                (StocksApplication) getActivity().getApplication();
        portfolioRepository = application.getWatchlistRepo();
    }
}

