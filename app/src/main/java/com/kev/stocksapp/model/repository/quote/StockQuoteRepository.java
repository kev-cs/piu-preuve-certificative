package com.kev.stocksapp.model.repository.quote;

import android.text.TextUtils;
import android.util.Log;

import com.kev.stocksapp.model.Quote;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StockQuoteRepository {

    private static final String TAG = StockQuoteRepository.class
            .getSimpleName();
    private static final String SINGLE_SYMBOL_SELECT =
            "select * from yahoo.finance.quotes where symbol = \"%s\"";
    private static final String MULTI_SYMBOLS_SELECT =
            "select * from yahoo.finance.quotes where symbol in (\"%s\")";
    private static final String FORMAT = "json";
    private static final String ENV_STORE =
            "store://datatables.org/alltableswithkeys";

    private RetrofitQuoteAPI retrofitQuoteAPI;

    public StockQuoteRepository(RetrofitQuoteAPI retrofitQuoteAPI){
        this.retrofitQuoteAPI = retrofitQuoteAPI;
    }

    public void getSingleQuoteFromSymbol(String symbol,
            final StockQuoteRepoCallback<Quote> callback) throws IOException {
        String query = String.format(SINGLE_SYMBOL_SELECT, symbol.toUpperCase());
        Call<YahooQLResponse> call =
                retrofitQuoteAPI.getQuote(query, FORMAT, ENV_STORE);

        call.enqueue(new Callback<YahooQLResponse>() {
            @Override
            public void onResponse(Call<YahooQLResponse> call,
                    Response<YahooQLResponse> response) {
                YahooQLResponse.Result results = response.body().query.results;
                if (results != null) {
                    Quote quote = results.quote.get(0);
                    if (!TextUtils.isEmpty(quote.getName())) {
                        callback.onResponse(quote);
                        return;
                    }
                }
                callback.notFound();
            }

            @Override
            public void onFailure(Call<YahooQLResponse> call, Throwable t) {
                Log.e(TAG, t.getMessage(), t);
                callback.onFailure();
            }
        });
    }

    public void getQuotesFromSymbols(List<String> symbols,
            final StockQuoteRepoCallback<List<Quote>> callback) {
        List<String> upperCaseSymbols = new ArrayList<>();
        for(String symbol : symbols){
            upperCaseSymbols.add(symbol.toUpperCase());
        }
        String symbolsString = TextUtils.join(",", upperCaseSymbols);
        String query = String.format(MULTI_SYMBOLS_SELECT, symbolsString);
        Call<YahooQLResponse> call =
                retrofitQuoteAPI.getQuote(query, FORMAT, ENV_STORE);

        call.enqueue(new Callback<YahooQLResponse>() {
            @Override
            public void onResponse(Call<YahooQLResponse> call,
                    Response<YahooQLResponse> response) {
                YahooQLResponse.Result results = response.body().query.results;
                if (results != null) {
                    Quote quote = results.quote.get(0);
                    if (!TextUtils.isEmpty(quote.getName())) {
                        callback.onResponse(results.quote);
                        return;
                    }
                }
                callback.notFound();
            }

            @Override
            public void onFailure(Call<YahooQLResponse> call, Throwable t) {
                Log.e(TAG, t.getMessage(), t);
                callback.onFailure();
            }
        });
    }

}
