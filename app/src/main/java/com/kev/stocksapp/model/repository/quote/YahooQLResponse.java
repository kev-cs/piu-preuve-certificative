package com.kev.stocksapp.model.repository.quote;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

import com.kev.stocksapp.model.Quote;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Classe de data structure pour recevoir et convertir la reponse du site de
 * base de donnees Yahoo.
 */
public class YahooQLResponse {

    class ResultAdapter extends TypeAdapter<Result> {

        @Override
        public void write(JsonWriter out, Result value)
                throws IOException {

        }

        @Override
        public Result read(JsonReader in) throws IOException {
            Gson gson = new Gson();
            Result result = new Result();
            List<Quote> quoteList = new ArrayList<>();
            in.beginObject();
            in.nextName();

            if (in.peek() == JsonToken.BEGIN_ARRAY) {
                quoteList.addAll(Arrays.asList(
                        (Quote[]) gson.fromJson(in, Quote[].class)));
            } else {
                quoteList.add((Quote) gson.fromJson(in, Quote.class));
            }
            in.endObject();
            result.quote = quoteList;
            return result;
        }
    }

    public Query query;

    class Query {
        int count;
        String created;
        String lang;
        @JsonAdapter(ResultAdapter.class)
        Result results;
    }

    class Result {
        List<Quote> quote;
    }
}
