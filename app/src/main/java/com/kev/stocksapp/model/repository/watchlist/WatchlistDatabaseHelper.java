package com.kev.stocksapp.model.repository.watchlist;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class WatchlistDatabaseHelper extends SQLiteOpenHelper {
    private static final String TAG = "WatchlistDatabaseHelper";
    private static final String TABLE_NAME = "Watchlist";
    public static final String INSERT_QUERY =
            "INSERT OR REPLACE INTO " + TABLE_NAME + " VALUES ('%s')";
    private static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "MyStocksDb";

    private static final String SQL_CREATE_TABLE_STOCK =
            "CREATE TABLE IF NOT EXISTS " + TABLE_NAME +
                    "(symbol VARCHAR CONSTRAINT pk_id PRIMARY KEY)";

    public WatchlistDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.i(TAG, "Creating database");
        db.execSQL(SQL_CREATE_TABLE_STOCK);
    }

    public void save(String symbol) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL(String.format(INSERT_QUERY, symbol));
    }

    public List<String> loadWatchlist() {
        SQLiteDatabase db = getReadableDatabase();
        List<String> watchlist = new ArrayList<>();

        Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_NAME, null);
        while (cursor.moveToNext()) {
            String symbol = cursor.getString(cursor.getColumnIndex("symbol"));
            watchlist.add(symbol);
        }

        return watchlist;
    }

    public void removeQuote(String symbol) {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_NAME + " WHERE symbol = '" +
                symbol + "'");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.i(TAG, "Upgrading database from version " + oldVersion + " to "
                + newVersion);
        switch (oldVersion) {
            case 1:
                break;
            default:
                throw new IllegalStateException(
                        "onUpgrade() with unknown oldVersion " + oldVersion);
        }
    }
}