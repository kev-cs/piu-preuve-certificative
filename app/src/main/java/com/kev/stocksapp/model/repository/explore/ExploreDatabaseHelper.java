package com.kev.stocksapp.model.repository.explore;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExploreDatabaseHelper extends SQLiteOpenHelper {
    private static final String TAG = "ExploreDatabaseHelper";
	private static final int DATABASE_VERSION = 1;
	private static final String TABLE_NAME = "EXPLORE_TYPES";
	public static final String DATABASE_NAME = "MyStocksExploreTypes";

	private static final String SQL_CREATE_TABLE_EXPLORE =
			"CREATE TABLE IF NOT EXISTS " + TABLE_NAME +
					" (symbol VARCHAR, type VARCHAR)";
    public static final String INSERT_QUERY = "INSERT INTO "+TABLE_NAME
            +" VALUES ('%s', '%s')";

    public ExploreDatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		Log.i(TAG, "Creating database");
		db.execSQL(SQL_CREATE_TABLE_EXPLORE);
        populate(db);
	}

    private void populate(SQLiteDatabase db) {
        String[] top10 = new String[] {"GOOG", "YHOO", "TSLA", "MSFT",
                "AAPL", "FB", "TWTR", "AMZN", "INTC", "CSCO"};
        String[] majorIndices = new String[] {"^NYA", "^IXIC", "^NDX",
                "^GSPC", "^DJI", "^DJT", "^DJU", "^DJU"};
        String[] majorCurrencies = new String[] {"USDCAD=X", "EURUSD=X",
                "AUDUSD=X", "GBPUSD=X", "USDJPY=X", "EURJPY=X", "EURGBP=X",
                "USDCHF=X"};
        for(String symbol : top10) {
            db.execSQL(String.format(INSERT_QUERY, symbol, "Top10"));
        }
        for(String symbol : majorCurrencies) {
            db.execSQL(String.format(INSERT_QUERY, symbol, "Major Currencies"));
        }
        for(String symbol : majorIndices) {
            db.execSQL(String.format(INSERT_QUERY, symbol, "Major Indices"));
        }
    }

    public Map<String, List<String>> getExploreTypesLists(){
        SQLiteDatabase db = getReadableDatabase();
        Map<String, List<String>> exploreTypesLists = new HashMap<>();
        Cursor cursor = db.rawQuery("SELECT * from " + TABLE_NAME, null);
        while(cursor.moveToNext()){
            String symbol = cursor.getString(cursor.getColumnIndex("symbol"));
            String type = cursor.getString(cursor.getColumnIndex("type"));
            if(!exploreTypesLists.containsKey(type)){
                exploreTypesLists.put(type, new ArrayList<String>());
            }
            exploreTypesLists.get(type).add(symbol);
        }
        return exploreTypesLists;
    }

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.i(TAG, "Upgrading database from version " + oldVersion + " to " + newVersion);
		switch(oldVersion) {
			case 1:
				break;
			default:
				throw new IllegalStateException(
						"onUpgrade() with unknown oldVersion " + oldVersion);
		}
	}
}