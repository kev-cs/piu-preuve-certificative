package com.kev.stocksapp.model.repository.quote;

public interface StockQuoteRepoCallback<T> {

    void onResponse(T quotes);

    void notFound();

    void onFailure();
}
