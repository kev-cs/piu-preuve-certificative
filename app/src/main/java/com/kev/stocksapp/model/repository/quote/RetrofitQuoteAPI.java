package com.kev.stocksapp.model.repository.quote;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RetrofitQuoteAPI {

    String REST_PATH = "/v1/public/yql";

    @GET(REST_PATH)
    Call<YahooQLResponse> getQuote(@Query("q") String query, @Query("format")
            String format, @Query("env") String store);
}
