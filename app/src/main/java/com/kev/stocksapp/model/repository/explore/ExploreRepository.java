package com.kev.stocksapp.model.repository.explore;

import java.util.List;
import java.util.Map;

public class ExploreRepository {

    private ExploreDatabaseHelper databaseHelper;

    public ExploreRepository(ExploreDatabaseHelper databaseHelper){
        this.databaseHelper = databaseHelper;
    }

    /**
     * Retourne une map contenant les titres des typesde quotes en tant que
     * clef et la liste de symboles (strings) associe avec cette clef.
     * @return
     */
    public Map<String, List<String>> getExploreTypesLists() {
        return databaseHelper.getExploreTypesLists();
    }

}
