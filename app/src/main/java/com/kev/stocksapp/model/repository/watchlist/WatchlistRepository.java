package com.kev.stocksapp.model.repository.watchlist;

import java.util.List;
import java.util.Observable;

public class WatchlistRepository extends Observable {

    private List<String> watchlist;
    private WatchlistDatabaseHelper databaseHelper;

    public WatchlistRepository(WatchlistDatabaseHelper databaseHelper) {
        this.databaseHelper = databaseHelper;
        watchlist = databaseHelper.loadWatchlist();
    }

    public List<String> getWatchlistItemList() {
        return watchlist;
    }

    public void removePortfolioItem(String symbol) {
        watchlist.remove(symbol);
        databaseHelper.removeQuote(symbol);
        setChanged();
        notifyObservers();
    }

    public void addPortfolioItem(String symbol) {
        databaseHelper.save(symbol);
        watchlist.add(symbol);
        setChanged();
        notifyObservers();
    }
}
