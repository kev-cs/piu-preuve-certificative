package com.kev.stocksapp.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.math.BigDecimal;

public class Quote implements Serializable, Parcelable{
	private String symbol;
	private String Name;
	private String Currency;
	private String StockExchange;

	private BigDecimal Ask = BigDecimal.ZERO;
	private BigDecimal Bid = BigDecimal.ZERO;
	private String Change;
    private BigDecimal PreviousClose = BigDecimal.ZERO;
    private BigDecimal Open = BigDecimal.ZERO;
	private BigDecimal DaysLow = BigDecimal.ZERO;
	private BigDecimal DaysHigh = BigDecimal.ZERO;
	private BigDecimal YearLow = BigDecimal.ZERO;
	private BigDecimal YearHigh = BigDecimal.ZERO;
	private BigDecimal Volume = BigDecimal.ZERO;
    private BigDecimal LastTradePriceOnly = BigDecimal.ZERO;
    private String ChangeinPercent;

	public Quote() {
	}

	private Quote(Parcel in) {
        symbol = in.readString();
        Name = in.readString();
        Currency = in.readString();
        StockExchange = in.readString();

        Ask = (BigDecimal) in.readSerializable();
        Bid = (BigDecimal) in.readSerializable();
        Change = in.readString();
        PreviousClose = (BigDecimal) in.readSerializable();
        Open = (BigDecimal) in.readSerializable();
        DaysLow = (BigDecimal) in.readSerializable();
        DaysHigh = (BigDecimal) in.readSerializable();
        YearLow = (BigDecimal) in.readSerializable();
        YearHigh = (BigDecimal) in.readSerializable();
        Volume = (BigDecimal) in.readSerializable();
        LastTradePriceOnly = (BigDecimal) in.readSerializable();
        ChangeinPercent = in.readString();
	}

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getCurrency() {
        return Currency;
    }

    public void setCurrency(String currency) {
        Currency = currency;
    }

    public String getStockExchange() {
        return StockExchange;
    }

    public void setStockExchange(String stockExchange) {
        StockExchange = stockExchange;
    }

    public BigDecimal getAsk() {
        return Ask != null ? Ask : BigDecimal.ZERO;
    }

    public void setAsk(BigDecimal ask) {
        Ask = ask;
    }

    public BigDecimal getBid() {
        return Bid != null ? Bid : BigDecimal.ZERO;
    }

    public void setBid(BigDecimal bid) {
        Bid = bid;
    }

    public String getChange() {
        return Change;
    }

    public void setChange(String change) {
        Change = change;
    }

    public BigDecimal getPreviousClose() {
        return PreviousClose != null ? PreviousClose : BigDecimal.ZERO;
    }

    public void setPreviousClose(BigDecimal previousClose) {
        PreviousClose = previousClose;
    }

    public BigDecimal getOpen() {
        return Open != null ? Open : BigDecimal.ZERO;
    }

    public void setOpen(BigDecimal open) {
        Open = open;
    }

    public BigDecimal getDaysLow() {
        return DaysLow != null ? DaysLow : BigDecimal.ZERO;
    }

    public void setDaysLow(BigDecimal daysLow) {
        DaysLow = daysLow;
    }

    public BigDecimal getDaysHigh() {
        return DaysHigh != null ? DaysHigh : BigDecimal.ZERO;
    }

    public void setDaysHigh(BigDecimal daysHigh) {
        DaysHigh = daysHigh;
    }

    public BigDecimal getYearLow() {
        return YearLow != null ? YearLow : BigDecimal.ZERO;
    }

    public void setYearLow(BigDecimal yearLow) {
        YearLow = yearLow;
    }

    public BigDecimal getYearHigh() {
        return YearHigh != null ? YearHigh : BigDecimal.ZERO;
    }

    public void setYearHigh(BigDecimal yearHigh) {
        YearHigh = yearHigh;
    }

    public BigDecimal getVolume() {
        return Volume != null ? Volume : BigDecimal.ZERO;
    }

    public void setVolume(BigDecimal volume) {
        Volume = volume;
    }

    public BigDecimal getLastTradePriceOnly() {
        return LastTradePriceOnly != null ? LastTradePriceOnly : BigDecimal.ZERO;
    }

    public void setLastTradePriceOnly(BigDecimal lastTradePriceOnly) {
        LastTradePriceOnly = lastTradePriceOnly;
    }

    public String getChangeinPercent() {
        return ChangeinPercent;
    }

    public void setChangeinPercent(String changeinPercent) {
        ChangeinPercent = changeinPercent;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(symbol);
        dest.writeString(Name);
        dest.writeString(Currency);
        dest.writeString(StockExchange);

        dest.writeSerializable(Ask);
        dest.writeSerializable(Bid);
        dest.writeString(Change);
        dest.writeSerializable(PreviousClose);
        dest.writeSerializable(Open);
        dest.writeSerializable(DaysLow);
        dest.writeSerializable(DaysHigh);
        dest.writeSerializable(YearLow);
        dest.writeSerializable(YearHigh);
        dest.writeSerializable(Volume);
        dest.writeSerializable(LastTradePriceOnly);
        dest.writeString(ChangeinPercent);
    }

    public static final Creator<Quote> CREATOR =
            new Creator<Quote>() {
                @Override
                public Quote createFromParcel(Parcel in) {
                    return new Quote(in);
                }

                @Override
                public Quote[] newArray(int size) {
                    return new Quote[size];
                }
            };

}
