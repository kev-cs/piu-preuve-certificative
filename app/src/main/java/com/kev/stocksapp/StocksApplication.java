package com.kev.stocksapp;

import android.app.Application;
import android.content.Context;

import com.kev.stocksapp.model.repository.explore.ExploreDatabaseHelper;
import com.kev.stocksapp.model.repository.explore.ExploreRepository;
import com.kev.stocksapp.model.repository.quote.RetrofitQuoteAPI;
import com.kev.stocksapp.model.repository.quote.StockQuoteRepository;
import com.kev.stocksapp.model.repository.watchlist.WatchlistDatabaseHelper;
import com.kev.stocksapp.model.repository.watchlist.WatchlistRepository;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class StocksApplication extends Application {

    private static final String URL = "https://query.yahooapis.com";
    private WatchlistRepository watchlistRepo;
    private StockQuoteRepository stockQuoteRepository;
    private ExploreRepository exploreRepository;

    @Override
    public void onCreate() {
        Context context = getApplicationContext();
        ExploreDatabaseHelper exploreDatabaseHelper =
                new ExploreDatabaseHelper(context);
        WatchlistDatabaseHelper watchlistDatabaseHelper =
                new WatchlistDatabaseHelper(context);

        this.watchlistRepo = new WatchlistRepository(watchlistDatabaseHelper);
        this.stockQuoteRepository = new StockQuoteRepository(buildQuoteRetrofitAPI());
        this.exploreRepository = new ExploreRepository(exploreDatabaseHelper);
        super.onCreate();
    }

    private RetrofitQuoteAPI buildQuoteRetrofitAPI() {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create()).build();
        return retrofit.create(RetrofitQuoteAPI.class);
    }

    public WatchlistRepository getWatchlistRepo() {
        return watchlistRepo;
    }

    public StockQuoteRepository getStockQuoteRepository() {
        return stockQuoteRepository;
    }

    public ExploreRepository getExploreRepository() {
        return exploreRepository;
    }
}
